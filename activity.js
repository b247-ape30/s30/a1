//  Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([
		{ $match: {onSale: true } },// to match all true in onSale:

		{$count: "fruitsOnSale" } // count all true in match and return the value called fruitsOnsale
	]);


/*
	count operator to count the total number of 
	fruits with stock more than or equal to  20.
*/

db.fruits.aggregate([
         {
             $match: {stock: {$gte: 20 } } // match stock value geater than & equal to 20
         },
         {   
             $count: "enoughStock" // count the value geater than & equal to 20 assigns the value to a field called enoughStock
             
         }
 ]);

// average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
    { $match: {onSale: true}}, // to match all true in onSale:
    { 
        $group: { _id: "$supplier_id", // get supplier_id and price avg and assigns the value to 'avg_price'
        avg_price: {$avg: "$price"
         }
       }
     },
   ]);

// max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
    { $match: {onSale: true}},
    { 
        $group: 
        	{ _id: "$supplier_id", 
            max_price: {$max: "$price"
         }
       }
     },
     {$sort: {_id: -1} }
   ]);

// min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
    { $match: {onSale: true}},
    { 
        $group: { _id: "$supplier_id", 
        min_price: {$min: "$price"
         }
       }
     },
     {$sort: {_id: -1} }
   ]);